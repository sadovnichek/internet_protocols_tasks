import argparse
import base64
import socket
import ssl
import sys

boundary = "boundary1"


def parse_config():
    with open("config") as file:
        lines = file.readlines()
        for i in range(0, len(lines)):
            tokens = lines[i].split(":")
            if tokens[0] == "To":
                destinations = tokens[1].split(",")
            if tokens[0] == "From":
                source_address = tokens[1].strip()
            elif tokens[0] == "Subject":
                subject = tokens[1].strip()
            elif tokens[0] == "Attachments":
                attachments = lines[i + 1:]
                break
    return source_address, destinations, subject, attachments


def get_plain_text():
    with open('message_text.txt') as file:
        return file.read()


def get_attachment(path):
    with open(path, "rb") as file:
        message = ""
        content = file.read()
        message += base64.b64encode(content).decode() + "\n"
        message += f"--{boundary}\n"
        return message


def get_message_headers(source, destinations, subject):
    destination = "\n\t".join(destinations)
    return f'''From: {source}
To: {destination}
Subject: {subject}
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary=\"{boundary}\"\n
--{boundary}
Content-Type: text/plain\n\n'''


def request(socket, request):
    socket.send((request + '\n').encode())
    recv_data = socket.recv(65535).decode()
    return recv_data


def parse_args():
    parser = argparse.ArgumentParser(description="SMTP client")
    parser.add_argument("--user", help="Specify username")
    parser.add_argument("--password", help="Specify password")
    return parser.parse_args()


def to_base64(data):
    return base64.b64encode(data.encode()).decode()


def get_content_type(filename):
    extension = filename.split(".")[1]
    if extension == "png":
        return "image/png"
    elif extension == "pdf":
        return "application/pdf"
    elif extension in ["doc", "docx"]:
        return "application/msword"


def get_content_disposition(filename):
    filename = filename.split("/")[-1]
    return f'''Content-Disposition: attachment;
\tfilename="{filename}"
Content-Transfer-Encoding: base64
Content-Type: {get_content_type(filename)};
\tname="{filename}"\n\n'''


if __name__ == "__main__":
    source, destinations, subject, attachments = parse_config()
    attachments = list(map(lambda s: s.strip(), attachments))
    destinations = list(map(lambda s: s.strip(), destinations))
    args = parse_args()
    host = 'smtp.yandex.ru'
    port = 465
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
        try:
            client.connect((host, port))
        except socket.error:
            print("check your internet connection")
            sys.exit()
        client = ssl.wrap_socket(client)
        print(client.recv(1024))
        print(request(client, f'ehlo {args.user}'))
        print(request(client, f'AUTH LOGIN'))
        print(request(client, to_base64(args.user)))
        print(request(client, to_base64(args.password)))
        print(request(client, f'MAIL FROM:{source}'))
        for dest in destinations:
            print(request(client, f"RCPT TO: {dest}"))
        print(request(client, 'DATA'))
        message = get_message_headers(source, destinations, subject)
        message += get_plain_text() + f"\n--{boundary}\n"
        for path in attachments:
            message += get_content_disposition(path) + get_attachment(path)
        message += "."
        print(request(client, message))
